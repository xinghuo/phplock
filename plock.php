<?php

class plock {

    /**
     * $lock_timeout 设置等待回旋次数
     * $lock_wait_func 设置等待机制,本例使用usleep+mt_rand随机等待,随机等待有利错开多个竞争
     * $add_func 这里设置添加锁标志机制,本例使用PHP的APC组件apc_add函数,apc_add设定锁占有上限为5s,避免永久占有
     * $del_func 这里设置删除锁标志机制,本例使用PHP的APC组件apc_delete函数
     */
    private $locks;
    private $lock_timeout = 200;
    private $lock_wait_func;
    private $add_func;
    private $del_func;
    //文件锁存放路径
    private $path = null;
    //文件句柄
    private $fp = null;
    //锁粒度,设置越大粒度越小
    private $hashNum = 100;
    //cache key
    private $name;

    /**
     * 
     * 内存锁与文件锁的开关
     * @var bool
     */
    public $is_memory = FALSE;

    public function __construct() {
        $this->add_func = function($mutex) {
                    return apc_add('sl:' . $mutex, 1, 5);
        };

        $this->del_func = function($mutex) {
                    return apc_delete('sl:' . $mutex);
        };

        $this->lock_wait_func = function() {
                    usleep(mt_rand(10000, 50000));
        };
    }

    public function __destruct() {
        $this->clean();
    }

    /**
     * 清除当前所有设置的锁,在当前的php进程中可以设置多个锁
     */
    public function clean() {
        if ($this->is_memory) {
            if ($this->locks) {
                foreach ($this->locks as $lock => $tmp)
                    call_user_func($this->del_func, $lock);
                $this->locks = null;
            }
        }
    }

    /**
     * 新建立一个锁
     * 首先会判断锁定标志是否已经定义，如果已锁定则判定为死锁
     * 其次使用apc共享内存方式add一个锁标志,如果失败则进入等待时间,直到超时
     */
    public function lock($mutex, $path = '') {
        if ($this->is_memory) {
            if (isset($this->locks[$mutex]) && $this->locks[$mutex] != null) {
                throw new Exception('Detected deadlock.');
                return false;
            }

            while (call_user_func($this->add_func, $mutex) == false) {
                //                        ++$i;
                //                        if($i > $this->lock_timeout)
                //                        {
                //                                throw new Exception('lock timeout.');
                //                                return false;
                //                        }
                call_user_func($this->lock_wait_func);
            }
            $this->locks[$mutex] = 1;
            return $mutex;
        } else {

            $this->path = $path . ($this->_mycrc32($mutex) % $this->hashNum) . '.txt';
            $this->name = $mutex;
            //配置目录权限可写
            $this->fp = fopen($this->path, 'w+');
            if ($this->fp === false) {
                return false;
            }
            return flock($this->fp, LOCK_EX);
        }
    }

    /**
     * 手动释放锁,一般不用,
     * 在当前对象析构时会自动释放所有锁
     */
    public function release($mutex) {
        if ($mutex == false)
            return false;
        if ($this->is_memory) {
            unset($this->locks[$mutex]);
            call_user_func($this->del_func, $mutex);
            return true;
        } else {
            if ($this->fp !== false) {
                flock($this->fp, LOCK_UN);
                clearstatcache();
            }
            //进行关闭
            fclose($this->fp);
        }
    }

    /**
     * crc32
     * crc32封装
     * @param int $string
     * @Return int
     */
    private function _mycrc32($string) {
        $crc = abs(crc32($string));
        if ($crc & 0x80000000) {
            $crc ^= 0xffffffff;
            $crc += 1;
        }
        return $crc;
    }

}